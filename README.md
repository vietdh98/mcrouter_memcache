# Introducing mcrouter: A memcached protocol router for scaling memcached deployment 
![1](/uploads/9f9cf6f9b5de7bfe4b5d03c98f0c4a4f/1.jpg)

* Mcrouter là một bộ định tuyến giao thức memcached được sử dụng tại Facebook để xử lý tất cả lưu lượng truy cập đến, đi và giữa hàng nghìn máy chủ bộ nhớ cache trên hàng chục cụm được phân phối trong các trung tâm dữ liệu của Facebook trên khắp thế giới. Nó được chứng minh ở quy mô lớn - lúc cao điểm, mcrouter xử lý gần 5 tỷ yêu cầu mỗi giây. Mcrouter cũng đã được chứng minh là hoạt động như một tệp nhị phân độc lập trong thiết lập Amazon Web Services khi Instagram sử dụng nó trước khi chuyển đổi hoàn toàn sang cơ sở hạ tầng của Facebook.

## Features 
* Vì bất kỳ client nào muốn nói chuyện với memcached đều có thể nói chuyện qua giao thức **memcached ASCII** tiêu chuẩn, Facebook sử dụng giao thức đó làm API chung và nhập hình ảnh một cách im lặng. Đối với một client, mcrouter trông giống như một máy chủ memcached. Đối với một máy chủ, mcrouter trông giống như một client memcached bình thường. Nhưng khả năng cấu hình nhiều tính năng của mcrouter khiến nó không chỉ là một proxy đơn thuần.

* Một số tính năng của mcrouter được liệt kê dưới đây. Trong phần sau, *destination* là một máy chủ lưu trữ bộ nhớ đệm (memcached host) (hoặc một số dịch vụ bộ nhớ đệm khác hiểu giao thức bộ nhớ đệm) và *pool* là một tập hợp các điểm đến được định cấu hình cho một số khối lượng công việc - ví dụ: *pool* được phân đoạn với một hàm băm được chỉ định hoặc *pool* được sao chép với nhiều bản sao dữ liệu trên các máy chủ riêng biệt. Cuối cùng, *pools* có thể được tổ chức thành nhiều cụm.

![2](/uploads/4799c18838656d33f7ac1e66cbdcca5d/2.png)

* **Standard open source memcached ASCII protocol support**: Bất kỳ client nào có thể nói với giao thức memcached đều có thể nói chuyện với mcrouter - không cần thay đổi. Mcrouter có thể đơn giản được đặt vào giữa các client và các memcached box để tận dụng chức năng của nó.

* **Connection pooling**: Nhiều máy khách có thể kết nối với một instance mcrouter duy nhất và chia sẻ các kết nối gửi đi, giảm số lượng kết nối mở đến các instance memcached.

* **Multiple hashing schemes**: Mcrouter cung cấp một thuật toán băm nhất quán đã được chứng minh (furc_hash) cho phép phân phối các khóa trên nhiều instance memcached. Hàm băm tên máy chủ hữu ích để chọn một bản sao duy nhất cho mỗi máy khách. Có một số hàm băm khác hữu ích trong các ứng dụng chuyên biệt.

![3](/uploads/c591dee543afe50a6878b00eb1c436cb/3.png)

* **Prefix routing**: Mcrouter có thể định tuyến các khóa theo các tiền tố khóa chung. Ví dụ: bạn có thể gửi tất cả các khóa bắt đầu bằng “foo” đến một *pool*, tiền tố “bar” đến một *pool* khác và mọi thứ khác đến một "wildcard" *pool*. Đây là một cách đơn giản để tách các *workload* khác nhau.

![4](/uploads/6fca072a9a5aa8767509e35a3b5983b9/4.png)

* **Replicated pools**: Nhóm sao chép có cùng dữ liệu trên nhiều máy chủ. Các bản ghi được sao chép cho tất cả các máy chủ trong pool, trong khi các lần đọc được chuyển đến một bản sao duy nhất được chọn riêng cho từng client. Điều này có thể được thực hiện do các giới hạn của gói trên mỗi máy chủ lưu trữ trong đó một nhóm bị phân đoạn sẽ không thể xử lý tốc độ đọc; hoặc để tăng tính khả dụng của dữ liệu (một bản sao bị hỏng không ảnh hưởng đến tính khả dụng do tự động chuyển đổi dự phòng).

![5](/uploads/dd354977d2caa15e277656746e35190c/5.png)

* **Production traffic shadowing**: Khi thử nghiệm phần cứng bộ nhớ đệm mới, chúng tôi nhận thấy việc định tuyến bản sao hoàn chỉnh của lưu lượng sản xuất từ ​​các client là cực kỳ hữu ích. Mcrouter supports flexible shadowing. Có thể kiểm tra tối đa kích thước nhóm khác nhau (băm lại không gian khóa), chỉ phủ bóng một phần nhỏ của không gian chính hoặc thay đổi cài đặt tạo bóng động trong thời gian chạy.

* **Online reconfiguration**: Mcrouter giám sát các tệp cấu hình của nó và tự động reload chúng khi có bất kỳ thay đổi nào về tệp; quá trình loading và parsing này được thực hiện trên one thread background và các yêu cầu mới được định tuyến theo cấu hình mới ngay khi nó sẵn sàng. Không có thêm độ trễ theo quan điểm của client.

* **Flexible routing**: Cấu hình được chỉ định dưới dạng đồ thị của các mô-đun định tuyến nhỏ được gọi là "các chốt định tuyến", chia sẻ một giao diện chung (route a request and return a reply) và có thể được soạn thảo một cách tự do. Các chốt định tuyến dễ hiểu, dễ tạo và kiểm tra riêng lẻ, cho phép logic phức tạp tùy ý khi được sử dụng cùng nhau. Ví dụ: Một chốt định tuyến “đồng bộ hóa tất cả” sẽ được thiết lập với nhiều chốt định tuyến con (bản thân chúng có thể là những chốt định tuyến tùy ý). Nó sẽ chuyển một yêu cầu đến tất cả các con của nó và đợi tất cả các câu trả lời gửi lại trước khi trả lại một trong những câu trả lời này. Các ví dụ khác bao gồm, trong số nhiều ví dụ khác, "all-async" (gửi cho tất cả nhưng không đợi trả lời), "all-majority" (để bỏ phiếu đồng thuận) và "chuyển đổi dự phòng" (gửi cho mọi trẻ em theo thứ tự cho đến khi trả lời không lỗi được trả lại.

* **Destination health monitoring and automatic failover**: Mcrouter theo dõi tình trạng sức khỏe của từng destination. Nếu mcrouter đánh dấu một host là không phản hồi, nó sẽ tự động thực hiện chuyển các yêu cầu đến một host khác thay thế (chuyển đổi dự phòng nhanh) mà không cố gắng gửi chúng đến host ban đầu. Đồng thời, các yêu cầu kiểm tra sức khỏe sẽ được gửi trong nền và ngay sau khi kiểm tra sức khỏe thành công, mcrouter sẽ trở lại sử dụng destination ban đầu. Facebook phân biệt giữa "soft error" (ví dụ: hết thời gian chờ dữ liệu) được phép xảy ra một vài lần liên tiếp và "hard error" (ví dụ: kết nối bị từ chối) khiến máy chủ được đánh dấu là không phản hồi ngay lập tức.

![6](/uploads/67773f2a02477224d8573a5653b49b6e/6.png)

![7](/uploads/62961db4fb0b2760b7b8c5e47b12d89c/7.png)

* **Cold cache warm up**: Mcrouter có thể làm trơn tru tác động hiệu suất của việc khởi động một máy chủ lưu trữ bộ nhớ cache trống hoàn toàn mới hoặc một tập hợp các máy chủ (lớn như toàn bộ cụm) bằng cách tự động lấp đầy nó từ một bộ nhớ cache “ấm” được chỉ định.

![8](/uploads/b780740722012979e1fa36c79d43b9e4/8.png)

* **Broadcast operations**: Bằng cách thêm tiền tố đặc biệt vào khóa trong một yêu cầu, thật dễ dàng sao chép cùng một yêu cầu thành nhiều pools hoặc cụm. 

![9](/uploads/2092b9b8b88b6cba31648d5818498551/9.png)

* **Reliable delete stream**: Trong bộ đệm ẩn nhìn sang một bên được đáp ứng đầy đủ nhu cầu, điều quan trọng là phải đảm bảo tất cả các lần xóa cuối cùng đều được phân phối để đảm bảo tính nhất quán. Mcrouter hỗ trợ ghi nhật ký các lệnh xóa vào đĩa trong trường hợp không thể truy cập đích (do sự cố mạng hoặc lỗi khác). Sau đó, một quy trình riêng biệt sẽ phát lại những lần xóa không đồng bộ đó. Việc này được thực hiện một cách minh bạch với client - lệnh xóa ban đầu luôn được báo cáo là thành công.

![10](/uploads/d4fd95c448d49bd27426f1f29088487f/10.png)

* **Multi-cluster support**: Dễ dàng quản lý cấu hình cho các thiết lập nhiều cụm lớn. Một cấu hình duy nhất có thể được phân phối cho tất cả các cụm và tùy thuộc vào các tùy chọn dòng lệnh, mcrouter sẽ diễn giải cấu hình dựa trên vị trí của nó.
  
* **Rich stats and debug commands**: Mcrouter xuất nhiều bộ đếm nội bộ (thông qua lệnh “thống kê”; cũng như tệp JSON trên đĩa). Các lệnh gỡ lỗi nội tâm cũng có sẵn, có thể trả lời các câu hỏi như “Một yêu cầu cụ thể sẽ chuyển đến máy chủ nào?” trong thời gian chạy.

* **Quality of service**: Mcrouter cho phép điều chỉnh tốc độ của bất kỳ loại yêu cầu nào (ví dụ: get/set/delete) ở bất kỳ cấp độ nào (mỗi máy chủ lưu trữ, mỗi nhóm, mỗi cụm), từ chối các yêu cầu vượt quá một giới hạn cụ thể. Nó cũng hỗ trợ các yêu cầu giới hạn tỷ lệ để giao hàng chậm.

* **Large values**: Mcrouter có thể tự động tách / ghép lại các giá trị lớn mà thông thường sẽ không vừa với slab memcached.

* **Multi-level caches**: Mcrouter hỗ trợ thiết lập bộ đệm local/remote, nơi các giá trị sẽ được tra cứu local trước và tự động đặt trong local cache remote cache sau khi tìm nạp.
  
* **IPv6 support**: Chúng có hỗ trợ nội bộ mạnh mẽ cho IPv6 tại Facebook, vì vậy mcrouter tương thích với IPv6 ngay từ đầu.
  
* **SSL support**: Mcrouter hỗ trợ kết nối SSL (đến hoặc đi), miễn là client hoặc máy chủ đích cũng hỗ trợ nó. Cũng có thể thiết lập nhiều mcrouter nối tiếp nhau, trong trường hợp đó, kết nối giữa các bộ định tuyến có thể qua SSL.

* **Multi-threaded architecture**: Mcrouter có thể tận dụng tối đa các hệ thống đa lõi bằng cách bắt đầu một luồng cho mỗi lõi.